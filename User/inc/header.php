<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta content="IE=edge" http-equiv="X-UA-Compatible">
  <meta content="width=device-width,initial-scale=1" name="viewport">
  <meta content="description" name="description">
  <meta name="google" content="notranslate" />
  <meta content="Mashup templates have been developped by Orson.io team" name="author">

  <!-- Disable tap highlight on IE -->
  <meta name="msapplication-tap-highlight" content="no">
  
  <link href="./assets/images/Logo-large.png" rel="apple-touch-icon">
  <link rel="shortcut icon" href="./assets/images/favicon.png" />

 

  <title>Ahmedabad Live</title>  

<link href="./assets/main.a3f694c0.css" rel="stylesheet"></head>

<body>

 <!-- Add your content of header -->
<header>
  <nav class="navbar  navbar-fixed-top navbar-default">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle uarr collapsed" data-toggle="collapse" data-target="#navbar-collapse-uarr">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="./index.php" title="">
          <img src="./assets/images/Logo-Large.png" class="navbar-logo-img" alt="">
        </a>
      </div>

      <div class="collapse navbar-collapse" id="navbar-collapse-uarr">
        <ul class="nav navbar-nav navbar-right">
          <li><a href="index.php" title="" class="active">Home</a></li>
          <li><a href="about.php" title=""> About</a></li>
          <!-- <li><a href="./pricing.html" title=""> Pricing </a></li> -->
          <li><a href="complain.php" title="">Complains</a></li>
          <li><a href="contact.php" title="">Contact</a></li>
          <li><a href="./login.php" title="">Login/Signup</a></li>
          <!-- <li><a href="components.php" title="">Components</a></li> -->
         <!--  <li>
            <p>
              <a href="./download.html" class="btn btn-primary navbar-btn" title="">Download</a>
            </p>
          </li> -->
          
        </ul>
      </div>
    </div>
  </nav>
</header>