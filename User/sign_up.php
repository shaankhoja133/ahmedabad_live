<!DOCTYPE html>
<html>
    <head>
        <title>Sign Up</title>
        <link rel="stylesheet" type="text/css" href="./assets/css/FormDeco.css"/>
    </head>
    <body>
        <div class="popup" style="width: 450px; height: 690px; ;">
            <div class="close-btn">&times;</div>
            <div class="form">
                <h2>Sign Up</h2>
                <form action="./Signup.php" method="post" enctype="multipart/form-data">
                    <div class="form-element">
                        <label for="Uname">Enter your Name: </label>
                        <input type="text" name="UName" id="UName" placeholder="UserName"/>
                    </div>
                    <div class="form-element">
                        <label for="Dob">Enter your DOB: </label>
                        <input type="date" name="Dob" id="Dob"/>
                    </div>
                    <div class="form-element">
                        <label for="DEPT">Select your Zone: </label>
                        <!-- <script type="text/php"> -->
                                    <?php
                                        $conn = mysqli_connect("localhost", "root", "", "test");
                        
                                        // Check connection
                                        if($conn === false){
                                            die("ERROR: Could not connect. " 
                                                . mysqli_connect_error());
                                        }
                                        
                                        echo '<div class="form-element">
                                                    <select name="Zone" id="Zones">
                                                    <option value="Maninagar"> Maninagar </option> 
                                                    <option value="Naroda"> Naroda </option> 
                                                    <option value="Chandranagar"> Chandranagar </option> 
                                                    <option value="Behrampura"> Behrampura </option> 
                                                    <option value="Sharkhej"> Sharkhej </option>';

                                        $sql = "SELECT Z_Name from zones";
                                        $result = $conn-> query($sql);
                                        if ($result-> num_rows > 0){
                                          while ($row = $result-> fetch_assoc()){

                                                echo '<option value="'. $row['Z_Name'] .'"> '. $row['Z_Name'] .'</option>';    
                                              } 
                                              echo '</select>
                                                </div>';
                                             } else {
                                                echo '0 Result';
                                              }
                                            $conn-> close();
                                    ?>
                            <!-- </script> -->
                    </div>
                    <div class="form-element">
                        <label for="C_No">Enter your Contact No: </label>
                        <input type="text" name="C_No" id="C_No" placeholder="Contact No"/>
                    </div>
                    <div class="form-element">
                        <label for="Address">Enter your Address : </label>
                        <input type="text" name="Add" id="Add" rows='4' column='50' placeholder="Address"/>
                    </div>
                    <div class="form-element">
                        <label for="Pin">Enter your Pincode: </label>
                        <input type="text" name="Pin" id="Pin" placeholder="PinCode"/>
                    </div>
                    <div class="form-element">
                        <label for="Profile">Upload  Your Profile Picture: (44 x 44 only) </label>
                        <input type="file" name="Profile" id="Profile"/>
                    </div>
                    <div class="form-element">
                        <button type="submit" name="Sign_Up">Sign Up</button>
                    </div>
                </form>
            </div>
        </div>
    </body>
</html>