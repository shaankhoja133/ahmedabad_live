<?php
include'./inc/header.php'
?>


<div class="white-text-container background-image-container" style="background-image: url('./assets/images/Index Pic.jpg')">
    <div class="opacity"></div>
    <div class="container">
        <div class="row">
           
            <div class="col-md-6">
                <h1>Welcome To Ahmedabad Live</h1>
                <p><h4>Home of filing complains in digital way.</h4> </p>
                 <!-- <a href="./download.html" title="" class="btn btn-lg btn-primary">Download</a> -->
            </div>

        </div>
    </div>
</div>

<div class="section-container border-section-container">
    <div class="container">
            <div class="row">
                <div class="col-md-12 section-container-spacer">
                    <div class="text-center">
                        <h2>How to register a complain?</h2>
                        <p> It's  very simple. you just have to click on Complains which is on the header section of website. </p>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="fa-container">
                        <i class="fa fa-comment-o fa-3x" aria-hidden="true"></i>
                    </div>
                    <div class="text-center">
                        <h3>Comments</h3>
                    </div>
                    <div>
                        <p>Select and fill up the form in the complains section and  please check the form before submitting..</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="fa-container">
                        <i class="fa fa-heart-o fa-3x" aria-hidden="true"></i>
                    </div>
                    <div class="text-center">
                        <h3>Share it</h3>
                    </div>
                    <div>
                        <p>Please share the site to lots of people, So their issues can also be resolved.
                        </p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="fa-container">
                        <i class="fa fa-bell-o fa-3x" aria-hidden="true"></i>
                    </div>
                    <div class="text-center">
                        <h3>Alerts</h3>
                    </div>
                    <div>
                        <p>You will also get alerts for the complains you've filed in future once this site will gain more popularity. 
                        </p>
                    </div>
                </div>

            </div>
    </div>
</div>

<!-- <div class="section-container">
    <div class="container">
            <div class="row">
                <div class="col-xs-12">


                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                        
                        <div class="carousel-inner" role="listbox">
                            <div class="item active">
                                <img class="img-responsive" src="./assets/assets/images/img-05.jpg" alt="First slide">
                            </div>
                            <div class="item">
                                <img class="img-responsive" src="./assets/assets/images/img-06.jpg" alt="Second slide">
                            </div>
                            <div class="item">
                                <img class="img-responsive" src="./assets/assets/images/img-07.jpg" alt="Third slide">
                            </div>
                        </div>
                        <ol class="carousel-indicators">
                            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                        </ol>
                    </div>


                </div>
            </div>
    </div>
</div> -->


<!-- <div class="section-container background-color-container white-text-container">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="text-center">
                    <h2>Vivamus laoreet</h2>
                    <p> Auctor augue mauris augue neque. Posuere lorem ipsum dolor sit amet consectetur adipiscing. Porta non
                        pulvinar neque laoreet. Viverra ipsum nunc aliquet bibendum. Iaculis urna id volutpat lacus. Turpis
                        egestas pretium aenean pharetra magna ac. Id cursus metus aliquam eleifend mi. </p>
                    <!-- <a href="./download.html" title="" class="btn btn-primary btn-lg">Download</a> -->
                </div>
            </div>
        </div>
     </div>
 </div>

 <div class="section-container">
    <div class="container">
        <div class="row">                   
            <div class="col-md-7">
                <img class="img-responsive" src="./assets/images/img-05.jpg" alt="">
            </div>

            <div class="col-md-5">
                <ul class="features">
                    <li>
                        <h3>Create your User account</h3>
                        <p>By creating your user account, you will be able to register a complaint.
                        </p>
                    </li>
                    <li>
                        <h3>Registering Complaint</h3>
                        <p>After you create an account, you can select the department and fill the register form.
                        </p>
                    </li>
                    <li>
                        <h3>Complaint</h3>
                        <p>After registering the complaint, we will try to process the complaint as soon as possible.
                        </p>
                    </li>
                </ul>
            </div>
        

              
            <div class="row">
                <div class="col-md-4">
                        <img class="img-responsive page-base-image" src="./assets/assets/images/logo-01.png" alt="">

                </div>
                <div class="col-md-4">
                        <img class="img-responsive page-base-image" src="./assets/assets/images/logo-02.png" alt="">
                </div>
                <div class="col-md-4">
                        <img class="img-responsive page-base-image" src="./assets/assets/images/logo-03.png" alt="">
                </div>
            </div>
            
        </div>
    </div>
</div>









<?php
include'./inc/footer.php'
?>


