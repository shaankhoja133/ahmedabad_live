<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta content="IE=edge" http-equiv="X-UA-Compatible">
  <meta content="width=device-width,initial-scale=1" name="viewport">
  <meta content="description" name="description">
  <meta name="google" content="notranslate" />
  <meta content="Mashup templates have been developped by Orson.io team" name="author">

  <!-- Disable tap highlight on IE -->
  <meta name="msapplication-tap-highlight" content="no">
  
  <link href="./assets/assets/apple-touch-icon.png" rel="apple-touch-icon">
  <link href="./assets/assets/favicon.ico" rel="icon">

 

  <title>Ahmedabad Live</title>  

<link href="./assets/main.a3f694c0.css" rel="stylesheet">
<link rel="shortcut icon" href="./assets/images/favicon.png" />
</head>

<body>

 <!-- Add your content of header -->
<header>
  <nav class="navbar  navbar-fixed-top navbar-default">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle uarr collapsed" data-toggle="collapse" data-target="#navbar-collapse-uarr">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="./index.php" title="">
          <img src="./assets/images/Logo-Large.png" class="navbar-logo-img" alt="">
        </a>
      </div>

      <div class="collapse navbar-collapse" id="navbar-collapse-uarr">
        <ul class="nav navbar-nav navbar-right">
          <li><a href="./index.php" title="" class="active">Home</a></li>
          <li><a href="./about.php" title=""> About</a></li>
          <!-- <li><a href="./pricing.html" title=""> Pricing </a></li> -->
          <li><a href="./complain.php" title="">Complains</a></li>
          <li><a href="./contact.php" title="">Contact</a></li>
          <li><a href="./login.php" title="">Login/Signup</a></li>
          <!-- <li><a href="./components.php" title="">Components</a></li> -->
<!--           <li>
            <p>
              <a href="./download.html" class="btn btn-primary navbar-btn" title="">Download</a>
            </p>
          </li> -->
          
        </ul>
      </div>
    </div>
  </nav>
</header>




<div class="">
    <div class="container">
        <div class="row">

            <div class="col-xs-12">
               <img class="img-responsive" src="./assets/images/About_Us_river.jpg">
            </div>
        </div>
    </div>
</div>

<div class="section-container">
    <div class="container">
        <div class="row">
               <div class="col-xs-12 col-md-8 col-md-offset-2">
                    <div class="text-center">
                    <h1>About Us</h1>
                     </div>   
                        <p class="section-container-spacer">Hello User,I hope you find the site easy to operate. This site is made to make your complains register Digitally and make your work as easy as possible.
                          The main goal of making this site was to provide an easy complain registering system to the people of Ahmedabad.
                        </p>

                    <div class="row section-container-spacer">
                        <div class="col-md-6">
                            <img class="img-responsive" src="./assets/images/About_Us_Monuments.jpg">
                        </div>
                        <div class="col-md-6">
                            <img class="img-responsive" src="./assets/assets/images/img-02.jpg">
                        </div>
                    </div>

               </div>
               
                <div class="col-xs-12 col-md-8 col-md-offset-2">
              
                        <p>
                            It took us almost a year to make this site. We hope that it is an usefull source for you. And also we will continue providing more and more future enhancements and will try to give you more and more help as much as possible.  </p>
                            <small class="signature pull-right">AMD_Live</small>
                </div>
            </div>




        </div>

    </div>



</div>







<footer>
    <div class="section-container footer-container">
        <div class="container">
            <div class="row">
                    <div class="col-md-4">
                        <!-- <h4>About us</h4> -->
                        <!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sit amet consectetur dolor</p> -->
                    </div>

                    <!-- <div class="col-md-4">
                        <h4>Do you like ? Share this !</h4>
                        <p>
                            <a href="https://facebook.com/" class="social-round-icon white-round-icon fa-icon" title="">
                            <i class="fa fa-facebook" aria-hidden="true"></i>
                          </a>
                          <a href="https://twitter.com/" class="social-round-icon white-round-icon fa-icon" title="">
                            <i class="fa fa-twitter" aria-hidden="true"></i>
                          </a>
                          <a href="https://www.linkedin.com/" class="social-round-icon white-round-icon fa-icon" title="">
                            <i class="fa fa-linkedin" aria-hidden="true"></i>
                          </a>
                        </p>
                        <p><small>© Untitled | Website created with <a href="http://www.mashup-template.com/" class="link-like-text" title="Create website with free html template">Mashup Template</a>/<a href="http://www.unsplash.com/" class="link-like-text" title="Beautiful Free Images">Unsplash</a></small></p>    
                    </div> -->

                    <div class="col-md-4">
                        <h4>Give your review</h4>
                        
                        <div class="form-group">
                            <div class="input-group">
                                <input type="text" class="form-control footer-input-text">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-newsletter ">OK</button>
                                </div>
                            </div>
                        </div>


                    </div>
            </div>
        </div>
    </div>
</footer>

<script>
  document.addEventListener("DOMContentLoaded", function (event) {
    navActivePage();
  });
</script>

<!-- Google Analytics: change UA-XXXXX-X to be your site's ID 

<script>
  (function (i, s, o, g, r, a, m) {
    i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
      (i[r].q = i[r].q || []).push(arguments)
    }, i[r].l = 1 * new Date(); a = s.createElement(o),
      m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
  })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
  ga('create', 'UA-XXXXX-X', 'auto');
  ga('send', 'pageview');
</script>

--> 
<script type="text/javascript" src="./assets/main.41beeca9.js"></script></body>

</html>