<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Ahmedabad live User </title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="./assets/vendors/feather/feather.css">
  <link rel="stylesheet" href="./assets/vendors/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="./assets/vendors/ti-icons/css/themify-icons.css">
  <link rel="stylesheet" href="./assets/vendors/typicons/typicons.css">
  <link rel="stylesheet" href="./assets/vendors/simple-line-icons/css/simple-line-icons.css">
  <link rel="stylesheet" href="./assets/vendors/css/vendor.bundle.base.css">
  <!-- endinject -->
  <!-- Plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="./assets/css/vertical-layout-light/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="./assets/images/favicon.png" />
</head>

<body>
  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
      <div class="content-wrapper d-flex align-items-center auth px-0">
        <div class="row w-100 mx-0">
          <div class="col-lg-4 mx-auto">
            <div class="auth-form-light text-left py-5 px-4 px-sm-5">
              <div class="brand-logo">
                <center><img src="./assets/images/Logo-large.png" alt="logo"></center>
              </div>
              <table>
                  <tr>
                        <td>
                            <h4>Hello!  User. Already has an account? </h4>
                            <h6 class="fw-light"> Sign in continue. </h6>
                        </td>      
                        <!-- <td> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </td> -->
                        <td >
                            <h4>New Here! Create new account to continue. </h4>
                            <h6 class="fw-light">Sign Up to continue.</h6>
                        </td>
                    <tr >
                        <td>
                            <div class="mt-3">
                            <a class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn" href="./sign_in.php">SIGN IN</a>
                            </div>
                        </td>
                        <!-- <td> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </td> -->
                        <td>
                            <div class="mt-3">
                            <a class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn" href="./sign_up.php">SIGN UP</a>
                            </div>
                        </td>
                    </tr>
                </table>
                <!-- <div class="my-2 d-flex justify-content-between align-items-center"> -->
                  <!-- <a href="#" class="auth-link text-black">Forgot password?</a> -->
                </div>
                <div class="mb-2">
                  <!-- <button type="button" class="btn btn-block btn-facebook auth-form-btn"> -->
                    <!-- <i class="ti-facebook me-2"></i>Connect using facebook -->
                  </button>
                </div>
                <!-- <div class="text-center mt-4 fw-light"> -->
                  <!-- Don't have an account? <a href="register.html" class="text-primary">Create</a> -->
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- plugins:js -->
  <script src="./assets/vendors/js/vendor.bundle.base.js"></script>
  <!-- endinject -->
  <!-- Plugin js for this page -->
  <script src="./assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
  <!-- End plugin js for this page -->
  <!-- inject:js -->
  <script src="./assets/js/off-canvas.js"></script>
  <script src="./assets/js/hoverable-collapse.js"></script>
  <script src="./assets/js/template.js"></script>
  <script src="./assets/js/settings.js"></script>
  <script src="./assets/js/todolist.js"></script>
  <!-- endinject -->
</body>

</html>
