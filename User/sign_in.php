<!DOCTYPE html>
<html>
    <head>
        <title>Sign In</title>
        <link rel="stylesheet" type="text/css" href="./assets/css/FormDeco.css"/>
    </head>
    <body>
        <div class="popup">
            <div class="close-btn">&times;</div>
            <div class="form">
                <h2>Log in</h2>
                <form action="./Signin.php" method="post">
                    <div class="form-element">
                        <label for="Uname">Enter User Name: </label>
                        <input type="text" name="UName" id="UName" placeholder="UserName" required/>
                    </div>
                    <div class="form-element">
                        <label for="C_No">Select your Contact Number: </label>
                        <input type="text" name="C_No" id="C_No" placeholder="ContactNo" required/>
                    </div>
                    <div class="form-element">
                        <button type="submit" name="Sign_In">Sign In</button>
                    </div>
                </form>
            </div>
        </div>
    </body>
</html>