<?php

    $conn = mysqli_connect("localhost", "root", "", "test");
          
        // Check connection
    if($conn === false){
        die("ERROR: Could not connect. " 
            . mysqli_connect_error());
    }
    if(isset($_POST['submit'])){
           if(!empty($_POST['check_list'])) {
            // Counting number of checked checkboxes.
            $checked_count = count($_POST['check_list']);
               echo "You have selected following ".$checked_count." option(s): <br/>";
               // Loop to store and display values of individual checked checkbox.
                   foreach($_POST['check_list'] as $selected) {
                   echo "<p>".$selected ."</p>";
                   $sql = "UPDATE user SET isApproved='1' WHERE id= $selected";
                   if ($conn->query($sql) === TRUE) {
                   echo "Record updated successfully";
                   } else {
                       echo "Error updating record: " . $conn->error;
                    }
                
                    $conn->close();
                }
            }
        } else {
            echo "<b>Please Select Atleast One Option.</b>";
        }
        echo("Redirecting in 10 seconds.....");
        sleep(10);
        header("Location: dashboard.php");
        exit;
?>