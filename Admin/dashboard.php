<?php
include './inc/header.php';
include './inc/sidebar.php';
?>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
            <div class="col-sm-12">
              <div class="home-tab">
                <div class="d-sm-flex align-items-center justify-content-between border-bottom">
                  <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                      <!-- <a class="nav-link active ps-0" id="home-tab" data-bs-toggle="tab" href="#overview" role="tab" aria-controls="overview" aria-selected="true">Overview</a>
                    </li>
                    <li class="nav-item"> -->
                     <!--  <a class="nav-link" id="profile-tab" data-bs-toggle="tab" href="#audiences" role="tab" aria-selected="false">Audiences</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" id="contact-tab" data-bs-toggle="tab" href="#demographics" role="tab" aria-selected="false">Demographics</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link border-0" id="more-tab" data-bs-toggle="tab" href="#more" role="tab" aria-selected="false">More</a> -->
                    </li>
                  </ul>
                  <!-- <div>
                    <div class="btn-wrapper">
                      <a href="#" class="btn btn-otline-dark align-items-center"><i class="icon-share"></i> Share</a>
                      <a href="#" class="btn btn-otline-dark"><i class="icon-printer"></i> Print</a>
                      <a href="#" class="btn btn-primary text-white me-0"><i class="icon-download"></i> Export</a>
                    </div>
                  </div>
                </div> -->
                <div class="tab-content tab-content-basic">
                  <div class="tab-pane fade show active" id="overview" role="tabpanel" aria-labelledby="overview"> 
                   <!--  <div class="row">
                      <div class="col-sm-12">
                        <div class="statistics-details d-flex align-items-center justify-content-between">
                          <div>
                            <p class="statistics-title">Bounce Rate</p>
                            <h3 class="rate-percentage">32.53%</h3>
                            <p class="text-danger d-flex"><i class="mdi mdi-menu-down"></i><span>-0.5%</span></p>
                          </div>
                          <div>
                            <p class="statistics-title">Page Views</p>
                            <h3 class="rate-percentage">7,682</h3>
                            <p class="text-success d-flex"><i class="mdi mdi-menu-up"></i><span>+0.1%</span></p>
                          </div>
                          <div>
                            <p class="statistics-title">New Sessions</p>
                            <h3 class="rate-percentage">68.8</h3>
                            <p class="text-danger d-flex"><i class="mdi mdi-menu-down"></i><span>68.8</span></p>
                          </div>
                          <div class="d-none d-md-block">
                            <p class="statistics-title">Avg. Time on Site</p>
                            <h3 class="rate-percentage">2m:35s</h3>
                            <p class="text-success d-flex"><i class="mdi mdi-menu-down"></i><span>+0.8%</span></p>
                          </div>
                          <div class="d-none d-md-block">
                            <p class="statistics-title">New Sessions</p>
                            <h3 class="rate-percentage">68.8</h3>
                            <p class="text-danger d-flex"><i class="mdi mdi-menu-down"></i><span>68.8</span></p>
                          </div>
                          <div class="d-none d-md-block">
                            <p class="statistics-title">Avg. Time on Site</p>
                            <h3 class="rate-percentage">2m:35s</h3>
                            <p class="text-success d-flex"><i class="mdi mdi-menu-down"></i><span>+0.8%</span></p>
                          </div>
                        </div>
                      </div>
                    </div>  -->
                    <div class="row">
                      <!-- <div class="col-lg-8 d-flex flex-column">
                        <div class="row flex-grow">
                          <div class="col-12 col-lg-4 col-lg-12 grid-margin stretch-card">
                            <div class="card card-rounded">
                              <div class="card-body">
                                <div class="d-sm-flex justify-content-between align-items-start">
                                  <div>
                                   <h4 class="card-title card-title-dash">Performance Line Chart</h4>
                                   <h5 class="card-subtitle card-subtitle-dash">Lorem Ipsum is simply dummy text of the printing</h5>
                                  </div>
                                  <div id="performance-line-legend"></div>
                                </div>
                                <div class="chartjs-wrapper mt-5">
                                  <canvas id="performaneLine"></canvas>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div> -->
                     <!--  <div class="col-lg-4 d-flex flex-column">
                        <div class="row flex-grow">
                          <div class="col-md-6 col-lg-12 grid-margin stretch-card">
                            <div class="card bg-primary card-rounded">
                              <div class="card-body pb-0">
                                <h4 class="card-title card-title-dash text-white mb-4">Status Summary</h4>
                                <div class="row">
                                  <div class="col-sm-4">
                                    <p class="status-summary-ight-white mb-1">Closed Value</p>
                                    <h2 class="text-info">357</h2>
                                  </div>
                                  <div class="col-sm-8">
                                    <div class="status-summary-chart-wrapper pb-4">
                                      <canvas id="status-summary"></canvas>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-6 col-lg-12 grid-margin stretch-card">
                            <div class="card card-rounded">
                              <div class="card-body">
                                <div class="row">
                                  <div class="col-sm-6">
                                    <div class="d-flex justify-content-between align-items-center mb-2 mb-sm-0">
                                      <div class="circle-progress-width">
                                        <div id="totalVisitors" class="progressbar-js-circle pr-2"></div>
                                      </div>
                                      <div>
                                        <p class="text-small mb-2">Total Visitors</p>
                                        <h4 class="mb-0 fw-bold">26.80%</h4>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-sm-6">
                                    <div class="d-flex justify-content-between align-items-center">
                                      <div class="circle-progress-width">
                                        <div id="visitperday" class="progressbar-js-circle pr-2"></div>
                                      </div>
                                      <div>
                                        <p class="text-small mb-2">Visits per day</p>
                                        <h4 class="mb-0 fw-bold">9065</h4>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div> -->
                    </div>
                    <div class="row">
                      <div class="col-lg-8 d-flex flex-column">
                        <div class="row flex-grow">
                          <div class="col-12 grid-margin stretch-card">
                            <div class="card card-rounded">
                              <div class="card-body">
                                <div class="d-sm-flex justify-content-between align-items-start">
                                  <div>
                                    <h4 class="card-title card-title-dash">Complaints Overview</h4>
                                   <p class="card-subtitle card-subtitle-dash">This is the review of all the complaints that has been filled.</p>
                                  </div>
                                  <div>
                                    <div class="dropdown">
                                      <button class="btn btn-secondary dropdown-toggle toggle-dark btn-lg mb-0 me-0" type="button" id="dropdownMenuButton2" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> This month </button>
                                      <div class="dropdown-menu" aria-labelledby="dropdownMenuButton2">
                                        <h6 class="dropdown-header">Settings</h6>
                                        <a class="dropdown-item" href="#">Action</a>
                                        <a class="dropdown-item" href="#">Another action</a>
                                        <a class="dropdown-item" href="#">Something else here</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="#">Separated link</a>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="d-sm-flex align-items-center mt-1 justify-content-between">
                                  <div class="d-sm-flex align-items-center mt-4 justify-content-between"><h2 class="me-2 fw-bold">120 Complaints </h2><h4 class="text-success">(+1.37%)</h4></div>
                                  <div class="me-3"><div id="marketing-overview-legend"></div></div>
                                </div>
                                <div class="chartjs-bar-wrapper mt-3">
                                  <canvas id="marketingOverview"></canvas>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row flex-grow">
                          <div class="col-12 grid-margin stretch-card">
                          <div class="card card-rounded">
                              <div class="card-body">
                                <div class="row">
                                  <div class="col-lg-12">
                                    <div class="d-flex justify-content-between align-items-center">
                                      <h4 class="card-title card-title-dash">New Users to approve!</h4>
                                      <div class="add-items d-flex mb-0">
                                        <form action="ApproveUser.php" method='post'>
                                        <!-- <input type="text" class="form-control todo-list-input" placeholder="What do you need to do today?"> -->
                                          <div>
                                              <button type="submit" name="submit" style= " font-size: 18px;
                                                                                           height: 40px;
                                                                                           width: 80px;
                                                                                           background: #222;
                                                                                           color: #f5f5f5;
                                                                                           border-radius: 10px;
                                                                                           cursor: pointer;" >Approve</button>
                                          </div>
                                      </div>
                                    </div>
                                    <div class="list-wrapper">
                                      <ul class="todo-list todo-list-rounded">
                                            <?php
                                                $conn = mysqli_connect("localhost", "root", "", "test");
                                
                                                // Check connection
                                                if($conn === false){
                                                    die("ERROR: Could not connect. " 
                                                        . mysqli_connect_error());
                                                }

                                                $sql = "SELECT id, F_Name, L_Name, Zone from user where isApproved = 0 ";
                                                $result = $conn-> query($sql);
                                                if ($result-> num_rows > 0){
                                                  while ($row = $result-> fetch_assoc()){
                                                    echo ' <li class="d-block">
                                                            <div class="form-check w-100">
                                                              <label class="form-check-label">
                                                                <input class="checkbox" type="checkbox" name="check_list[]" value="'.$row['id'].'">'. $row['F_Name'].' '. $row['L_Name'] . ' <i class="input-helper rounded"></i>
                                                              </label>
                                                              <div class="d-flex mt-2">
                                                                <div class="ps-4 text-small me-3"> Zone : '. $row['Zone'] . '</div>
                                                                <div class="badge badge-opacity-warning me-3">'.date('d-M-Y').'</div>
                                                                <i class="mdi mdi-flag ms-2 flag-color"></i>
                                                              </div>
                                                            </div>
                                                          </li>';
                                                        }
                                                    echo '</ul>
                                                        </form>
                                                        </div>'; 
                                                } else {
                                                            echo '<center>
                                                                    <li class="d-block">
                                                                      <div class="form-check w-100">
                                                                        <label class="form-check-label">
                                                                          <input class="checkbox" type="checkbox" disabled> <h5>No new Users are there!!</h5> <i class="input-helper rounded"></i>
                                                                        </label>
                                                                    
                                                                        <div class="d-flex mt-2">
                                                                          <div class="ps-4 text-small me-3"> Last Checked on:</div>
                                                                          <center><div class="badge badge-opacity-warning me-3">'.date('d-M-Y').'</div> 
                                                                          <i class="mdi mdi-flag ms-2 flag-color"></i>
                                                                        </div>
                                                                      
                                                                      </div>
                                                                    </li>
                                                                  </center>
                                                                </ul>
                                                          </form>
                                                        </div>';
                                                        }
                                              $conn-> close();
                                            ?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row flex-grow">
                          <div class="col-12 grid-margin stretch-card">
                            <div class="card card-rounded">
                              <div class="card-body">
                                <div class="d-sm-flex justify-content-between align-items-start">
                                  <div>
                                    <h4 class="card-title card-title-dash">Pending User Complaints</h4>
                                   <p class="card-subtitle card-subtitle-dash">5 complaints are in process.</p>
                                  </div>
                                  <div>
                                    <button class="btn btn-primary btn-lg text-white mb-0 me-0" type="button"><i class="mdi mdi-account-plus"></i>Modify Complaints Status</button>
                                  </div>
                                </div>
                                <div class="table-responsive  mt-1">
                                  <table class="table select-table">
                                    <thead>
                                      <tr>
                                        <th>
                                          <div class="form-check form-check-flat mt-0">
                                            <label class="form-check-label">
                                              <!-- <input type="checkbox" class="form-check-input" aria-checked="false"><i class="input-helper"></i></label> -->
                                          </div>
                                        </th>
                                        <th>Users</th>
                                        <th>Against Departments</th>
                                        <th>Progress</th>
                                        <th>Status</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td>
                                          <div class="form-check form-check-flat mt-0">
                                            <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" aria-checked="false"><i class="input-helper"></i></label>
                                          </div>
                                        </td>
                                        <td>
                                          <div class="d-flex ">
                                            <img src="./assets/images/faces/face1.jpg" alt="">
                                            <div>
                                              <h6>Hank Cheef</h6>
                                              <p>Zone: Maninagar</p>
                                            </div>
                                          </div>
                                        </td>
                                        <td>
                                          <h6>AMC Water Department</h6>
                                          <p>Sewage leakage Problem</p>
                                        </td>
                                        <td>
                                          <div>
                                            <div class="d-flex justify-content-between align-items-center mb-1 max-width-progress-wrap">
                                              <p class="text-success">79%</p>
                                              <p>85/162</p>
                                            </div>
                                            <div class="progress progress-md">
                                              <div class="progress-bar bg-success" role="progressbar" style="width: 85%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                          </div>
                                        </td>
                                        <td><div class="badge badge-opacity-warning">In progress</div></td>
                                      </tr>
                                      <tr>
                                        <td>
                                          <div class="form-check form-check-flat mt-0">
                                            <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" aria-checked="false"><i class="input-helper"></i></label>
                                          </div>
                                        </td>
                                        <td>
                                          <div class="d-flex">
                                            <img src="./assets/images/faces/face2.jpg" alt="">
                                            <div>
                                              <h6>Augusta Book</h6>
                                              <p>Zone: Chandranagar</p>
                                            </div>
                                          </div>
                                        </td>
                                        <td>
                                          <h6>AMC Water Department</h6>
                                          <p>Tap Water Quality Problem</p>
                                        </td>
                                        <td>
                                          <div>
                                            <div class="d-flex justify-content-between align-items-center mb-1 max-width-progress-wrap">
                                              <p class="text-success">65%</p>
                                              <p>85/162</p>
                                            </div>
                                            <div class="progress progress-md">
                                              <div class="progress-bar bg-success" role="progressbar" style="width: 65%" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                          </div>
                                        </td>
                                        <td><div class="badge badge-opacity-warning">In progress</div></td>
                                      </tr>
                                      <tr>
                                        <td>
                                          <div class="form-check form-check-flat mt-0">
                                            <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" aria-checked="false"><i class="input-helper"></i></label>
                                          </div>
                                        </td>
                                        <td>
                                          <div class="d-flex">
                                            <img src="./assets/images/faces/face3.jpg" alt="">
                                            <div>
                                              <h6>Ray O'Sun</h6>
                                              <p>Zone: Behrampura</p>
                                            </div>
                                          </div>
                                        </td>
                                        <td>
                                          <h6>AMC Electricity Department</h6>
                                          <p>Electricity Shortage Problem</p>
                                        </td>
                                        <td>
                                          <div>
                                            <div class="d-flex justify-content-between align-items-center mb-1 max-width-progress-wrap">
                                              <p class="text-success">65%</p>
                                              <p>85/162</p>
                                            </div>
                                            <div class="progress progress-md">
                                              <div class="progress-bar bg-warning" role="progressbar" style="width: 38%" aria-valuenow="38" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                          </div>
                                        </td>
                                        <td><div class="badge badge-opacity-warning">In progress</div></td>
                                      </tr>
                                      <tr>
                                        <td>
                                          <div class="form-check form-check-flat mt-0">
                                            <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" aria-checked="false"><i class="input-helper"></i></label>
                                          </div>
                                        </td>
                                        <td>
                                          <div class="d-flex">
                                            <img src="./assets/images/faces/face4.jpg" alt="">
                                            <div>
                                              <h6>Colin Sick</h6>
                                              <p>Zone: Sarkhej</p>
                                            </div>
                                          </div>
                                        </td>
                                        <td>
                                          <h6>AMC Transportation Department</h6>
                                          <p>Extra AMTS Bus Stop requirement problem</p>
                                        </td>
                                        <td>
                                          <div>
                                            <div class="d-flex justify-content-between align-items-center mb-1 max-width-progress-wrap">
                                              <p class="text-success">65%</p>
                                              <p>85/162</p>
                                            </div>
                                            <div class="progress progress-md">
                                              <div class="progress-bar bg-danger" role="progressbar" style="width: 15%" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                          </div>
                                        </td>
                                        <td><div class="badge badge-opacity-danger">Pending</div></td>
                                      </tr>
                                      <tr>
                                        <td>
                                          <div class="form-check form-check-flat mt-0">
                                            <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" aria-checked="false"><i class="input-helper"></i></label>
                                          </div>
                                        </td>
                                        <td>
                                          <div class="d-flex">
                                            <img src="./assets/images/faces/face5.jpg" alt="">
                                            <div>
                                              <h6>Aida Bugg</h6>
                                              <p>Zone: Naroda</p>
                                            </div>
                                          </div>
                                        </td>
                                        <td>
                                          <h6>AMC Cleaning and Sanitization Department</h6>
                                          <p>Cleaning Crew Regulation Problem</p>
                                        </td>
                                        <td>
                                          <div>
                                            <div class="d-flex justify-content-between align-items-center mb-1 max-width-progress-wrap">
                                              <p class="text-success">65%</p>
                                              <p>85/162</p>
                                            </div>
                                            <div class="progress progress-md">
                                              <div class="progress-bar bg-success" role="progressbar" style="width: 65%" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                          </div>
                                        </td>
                                        <td><div class="badge badge-opacity-success">Completed</div></td>
                                      </tr>
                                      <?php
                                                $conn = mysqli_connect("localhost", "root", "", "test");
                                
                                                // Check connection
                                                if($conn === false){
                                                    die("ERROR: Could not connect. " 
                                                        . mysqli_connect_error());
                                                }

                                                $sql = "SELECT complains.Against, complains.Title, complains.Zone, user.F_Name, User.L_Name, user.Profile_Pic from complains INNER JOIN user ON complains.U_Id = user.id";
                                                $result = $conn-> query($sql);
                                                if ($result-> num_rows > 0){
                                                  while ($row = $result-> fetch_assoc()){
                                  
                                                        $n=rand( 1 , 100);
                                                        $n2=rand( 100 , 200);
                                                        $a=array("warning"=>"in progress","danger"=>"Pending","success" => "Completed");
                                                        
                                                        $temp=array_rand($a,1);
                                                        $fin = (strcmp($temp,"suceess") && strcmp($temp,"danger") ) ? (strcmp($temp,"warning") ? "Completed" : "in Progress") :"Pending";
                                                        
                                                        if (strcmp($row['Profile_Pic'],'')!=0 ){
                                                          echo '<tr>
                                                                  <td>
                                                                    <div class="form-check form-check-flat mt-0">
                                                                      <label class="form-check-label">
                                                                      <input type="checkbox" class="form-check-input" aria-checked="false"><i class="input-helper"></i></label>
                                                                    </div>
                                                                  </td>
                                                                  <td>
                                                                    <div class="d-flex">
                                                                      <img src="./assets/images/faces/'.$row['Profile_Pic'].'" alt="">';
                                                           
                                                        } else {
                                                              echo '<tr>
                                                              <td>
                                                                <div class="form-check form-check-flat mt-0">
                                                                  <label class="form-check-label">
                                                                  <input type="checkbox" class="form-check-input" aria-checked="false"><i class="input-helper"></i></label>
                                                                </div>
                                                              </td>
                                                              <td>
                                                                <div class="d-flex">
                                                                  <img src="./assets/images/faces/face3.jpg" alt="">';
                                                        }
							                                                  echo '<div>
                                                                        <h6>'.$row['F_Name'].' '.$row['L_Name'].'</h6>
                                                                        <p>Zone: '. $row['Zone'] .'</p>
                                                                      </div>
                                                                    </div>
                                                                  </td>
                                                                  <td>
                                                                    <h6>'. $row['Against'] .'</h6>
                                                                    <p>'. $row['Title'] .'</p>
                                                                  </td>
                                                                  <td>
                                                                    <div>
                                                                      <div class="d-flex justify-content-between align-items-center mb-1 max-width-progress-wrap">
                                                                        <p class="text-'. $temp .'">'. $n .'%</p>
                                                                        <p>'. $n .'/'. $n2 .'</p>
                                                                      </div>
                                                                      <div class="progress progress-md">
                                                                        <div class="progress-bar bg-'. $temp .'" role="progressbar" style="width: '. $n .'%" aria-valuenow="'. $n .'" aria-valuemin="0" aria-valuemax="100"></div>
                                                                      </div>
                                                                    </div>
                                                                  </td>
                                                                  <td><div class="badge badge-opacity-'. $temp .'">'. $fin .'</div></td>
                                                                </tr>';
                                                      }
                                                      echo '</tbody>
                                                        </table>'; 
                                                } else {
                                                    echo '</tbody>
                                                    </table>';
                                                }
                                                $conn-> close();
                                        ?>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                       <!--  <div class="row flex-grow">
                          <div class="col-md-6 col-lg-6 grid-margin stretch-card">
                            <div class="card card-rounded">
                              <div class="card-body card-rounded">
                                <h4 class="card-title  card-title-dash">Recent Events</h4>
                                <div class="list align-items-center border-bottom py-2">
                                  <div class="wrapper w-100">
                                    <p class="mb-2 font-weight-medium">
                                      Change in Directors
                                    </p>
                                    <div class="d-flex justify-content-between align-items-center">
                                      <div class="d-flex align-items-center">
                                        <i class="mdi mdi-calendar text-muted me-1"></i>
                                        <p class="mb-0 text-small text-muted">Mar 14, 2019</p>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="list align-items-center border-bottom py-2">
                                  <div class="wrapper w-100">
                                    <p class="mb-2 font-weight-medium">
                                      Other Events
                                    </p>
                                    <div class="d-flex justify-content-between align-items-center">
                                      <div class="d-flex align-items-center">
                                        <i class="mdi mdi-calendar text-muted me-1"></i>
                                        <p class="mb-0 text-small text-muted">Mar 14, 2019</p>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="list align-items-center border-bottom py-2">
                                  <div class="wrapper w-100">
                                    <p class="mb-2 font-weight-medium">
                                      Quarterly Report
                                    </p>
                                    <div class="d-flex justify-content-between align-items-center">
                                      <div class="d-flex align-items-center">
                                        <i class="mdi mdi-calendar text-muted me-1"></i>
                                        <p class="mb-0 text-small text-muted">Mar 14, 2019</p>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="list align-items-center border-bottom py-2">
                                  <div class="wrapper w-100">
                                    <p class="mb-2 font-weight-medium">
                                      Change in Directors
                                    </p>
                                    <div class="d-flex justify-content-between align-items-center">
                                      <div class="d-flex align-items-center">
                                        <i class="mdi mdi-calendar text-muted me-1"></i>
                                        <p class="mb-0 text-small text-muted">Mar 14, 2019</p>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                
                                <div class="list align-items-center pt-3">
                                  <div class="wrapper w-100">
                                    <p class="mb-0">
                                      <a href="#" class="fw-bold text-primary">Show all <i class="mdi mdi-arrow-right ms-2"></i></a>
                                    </p>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-6 col-lg-6 grid-margin stretch-card">
                            <div class="card card-rounded">
                              <div class="card-body">
                                <div class="d-flex align-items-center justify-content-between mb-3">
                                  <h4 class="card-title card-title-dash">Activities</h4>
                                  <p class="mb-0">20 finished, 5 remaining</p>
                                </div>
                                <ul class="bullet-line-list">
                                  <li>
                                    <div class="d-flex justify-content-between">
                                      <div><span class="text-light-green">Ben Tossell</span> assign you a task</div>
                                      <p>Just now</p>
                                    </div>
                                  </li>
                                  <li>
                                    <div class="d-flex justify-content-between">
                                      <div><span class="text-light-green">Oliver Noah</span> assign you a task</div>
                                      <p>1h</p>
                                    </div>
                                  </li>
                                  <li>
                                    <div class="d-flex justify-content-between">
                                      <div><span class="text-light-green">Jack William</span> assign you a task</div>
                                      <p>1h</p>
                                    </div>
                                  </li>
                                  <li>
                                    <div class="d-flex justify-content-between">
                                      <div><span class="text-light-green">Leo Lucas</span> assign you a task</div>
                                      <p>1h</p>
                                    </div>
                                  </li>
                                  <li>
                                    <div class="d-flex justify-content-between">
                                      <div><span class="text-light-green">Thomas Henry</span> assign you a task</div>
                                      <p>1h</p>
                                    </div>
                                  </li>
                                  <li>
                                    <div class="d-flex justify-content-between">
                                      <div><span class="text-light-green">Ben Tossell</span> assign you a task</div>
                                      <p>1h</p>
                                    </div>
                                  </li>
                                  <li>
                                    <div class="d-flex justify-content-between">
                                      <div><span class="text-light-green">Ben Tossell</span> assign you a task</div>
                                      <p>1h</p>
                                    </div>
                                  </li>
                                </ul>
                                <div class="list align-items-center pt-3">
                                  <div class="wrapper w-100">
                                    <p class="mb-0">
                                      <a href="#" class="fw-bold text-primary">Show all <i class="mdi mdi-arrow-right ms-2"></i></a>
                                    </p>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div> -->
                      </div>
                      
                      <div class="col-lg-4 d-flex flex-column">
                        <div class="row flex-grow">
                          <div class="col-12 grid-margin stretch-card">
                            <div class="card card-rounded">
                              <div class="card-body">
                                <div class="row">
                                  <div class="col-lg-12">
                                    <div class="d-flex justify-content-between align-items-center">
                                      <h4 class="card-title card-title-dash">Sub Admin Status</h4>
                                      <div class="add-items d-flex mb-0">
                                        <!-- <input type="text" class="form-control todo-list-input" placeholder="What do you need to do today?"> -->
                                        <!-- <button class="add btn btn-icons btn-rounded btn-primary todo-list-add-btn text-white me-0 pl-12p"><i class="mdi mdi-plus"></i></button> -->
                                      </div>
                                    </div>
                                    <div class="table-responsive  mt-1">
                                  <table class="table select-table">
                                    <thead>
                                      <tr>
                                        <!--<th>
                                          <div class="form-check form-check-flat mt-0">
                                            <label class="form-check-label">
                                               <input type="checkbox" class="form-check-input" aria-checked="false"><i class="input-helper"></i></label> 
                                          </div>
                                        </th>-->
                                        <th>Sub Admins</th>
                                       <!--  <th>Departments</th>
                                        <th>Progress</th> -->
                                        <th>Status</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <!-- <tr>
                                        <td>
                                          <div class="form-check form-check-flat mt-0">
                                            <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" aria-checked="false"><i class="input-helper"></i></label>
                                          </div>
                                        </td>
                                        <td>
                                          <div class="d-flex ">
                                            <img src="./assets/images/faces/face1.jpg" alt="">
                                            <div>
                                              <h6>Brandon Washington</h6>
                                              <p>Head Admin</p>
                                            </div>
                                          </div>
                                        </td>
                                        <td>
                                          <h6>AMC</h6>
                                          <p>Government Co-operation</p>
                                        </td>
                                        <td>
                                          <div>
                                            <div class="d-flex justify-content-between align-items-center mb-1 max-width-progress-wrap">
                                              <p class="text-success">79%</p>
                                              <p>85/162</p>
                                            </div>
                                            <div class="progress progress-md">
                                              <div class="progress-bar bg-success" role="progressbar" style="width: 85%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                          </div>
                                        </td>
                                        <td><div class="badge badge-opacity-warning">In progress</div></td>
                                      </tr> -->
                                      <tr>
                                        <!--<td>
                                          <div class="form-check form-check-flat mt-0">
                                            <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" aria-checked="false"><i class="input-helper"></i></label>
                                          </div>
                                        </td>-->
                                        <td>
                                          <div class="d-flex">
                                            <img src="./assets/images/faces/face2.jpg" alt="">
                                            <div>
                                              <h6>Laura Brooks</h6>
                                              <p>AMC Water Department</p>
                                            </div>
                                          </div>
                                        </td>
                                       <!--  <td>
                                          <h6>AMC Water Department</h6>
                                          <p>Government Department</p>
                                        </td>
                                        <td>
                                          <div>
                                            <div class="d-flex justify-content-between align-items-center mb-1 max-width-progress-wrap">
                                              <p class="text-success">65%</p>
                                              <p>85/162</p>
                                            </div>
                                            <div class="progress progress-md">
                                              <div class="progress-bar bg-success" role="progressbar" style="width: 65%" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                          </div>
                                        </td> -->
                                        <td><div class="badge badge-opacity-warning">In progress</div></td>
                                      </tr>
                                      <tr>
                                        <!--<td>
                                          <div class="form-check form-check-flat mt-0">
                                             <label class="form-check-label">
                                             <input type="checkbox" class="form-check-input" aria-checked="false"><i class="input-helper"></i></label> 
                                          </div>
                                        </td>-->
                                        <td>
                                          <div class="d-flex">
                                            <img src="./assets/images/faces/face3.jpg" alt="">
                                            <div>
                                              <h6>Wayne Murphy</h6>
                                              <p>AMC Electricity Department</p>
                                            </div>
                                          </div>
                                        </td>
                                       <!--  <td>
                                          <h6>AMC Electricity Department</h6>
                                          <p>Private Institution : Torrent</p>
                                        </td>
                                        <td>
                                          <div>
                                            <div class="d-flex justify-content-between align-items-center mb-1 max-width-progress-wrap">
                                              <p class="text-success">65%</p>
                                              <p>85/162</p>
                                            </div>
                                            <div class="progress progress-md">
                                              <div class="progress-bar bg-warning" role="progressbar" style="width: 38%" aria-valuenow="38" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                          </div>
                                        </td> -->
                                        <td><div class="badge badge-opacity-warning">In progress</div></td>
                                      </tr>
                                      <tr>
                                         <!--<td>
                                          <div class="form-check form-check-flat mt-0">
                                            <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" aria-checked="false"><i class="input-helper"></i></label>
                                          </div>
                                        </td>--> 
                                        <td>
                                          <div class="d-flex">
                                            <img src="./assets/images/faces/face4.jpg" alt="">
                                            <div>
                                              <h6>Matthew Bailey</h6>
                                              <p>AMC Transportation Department</p>
                                            </div>
                                          </div>
                                        </td>
                                        <!-- <td>
                                          <h6>AMC Transportation Department</h6>
                                          <p>Government Department</p>
                                        </td>
                                        <td>
                                          <div>
                                            <div class="d-flex justify-content-between align-items-center mb-1 max-width-progress-wrap">
                                              <p class="text-success">65%</p>
                                              <p>85/162</p>
                                            </div>
                                            <div class="progress progress-md">
                                              <div class="progress-bar bg-danger" role="progressbar" style="width: 15%" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                          </div>
                                        </td> -->
                                        <td><div class="badge badge-opacity-danger">Pending</div></td>
                                      </tr>
                                      <tr>
                                        <!--<td>
                                          <div class="form-check form-check-flat mt-0">
                                              <label class="form-check-label">
                                          </div>
                                             <input type="checkbox" class="form-check-input" aria-checked="false"><i class="input-helper"></i></label> -->
                                        </td>
                                        <td>
                                          <div class="d-flex">
                                            <img src="./assets/images/faces/face5.jpg" alt="">
                                            <div>
                                              <h6>Katherine Butler</h6>
                                              <p>AMC Cleaning and Sanitization Department</p>
                                            </div>
                                          </div>
                                        </td>
                                        <!-- <td>
                                          <h6>AMC Cleaning and Sanitization Department</h6>
                                          <p>Government Department</p>
                                        </td>
                                        <td>
                                          <div>
                                            <div class="d-flex justify-content-between align-items-center mb-1 max-width-progress-wrap">
                                              <p class="text-success">65%</p>
                                              <p>85/162</p>
                                            </div>
                                            <div class="progress progress-md">
                                              <div class="progress-bar bg-success" role="progressbar" style="width: 65%" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                          </div>
                                        </td> -->
                                        <td><div class="badge badge-opacity-success">Completed</div></td>
                                      </tr>
                                      <?php
                                        $conn = mysqli_connect("localhost", "root", "", "test");
                        
                                        // Check connection
                                        if($conn === false){
                                            die("ERROR: Could not connect. " 
                                                . mysqli_connect_error());
                                        }

                                        $sql = "SELECT F_Name, L_Name, Dept, Profile_Pic from sub_admins";
                                        $result = $conn-> query($sql);
                                        if ($result-> num_rows > 0){
                                          while ($row = $result-> fetch_assoc()){
                                            $a=array("warning"=>"in progress","danger"=>"Pending","success" => "Completed");
                                            
                                            $temp=array_rand($a,1);
                                            $fin = (strcmp($temp,"suceess") && strcmp($temp,"danger") ) ? (strcmp($temp,"warning") ? "Completed" : "in Progress") :"Pending";
                                            
                                            echo '  <!--<td>
                                                      <div class="form-check form-check-flat mt-0">
                                                      <label class="form-check-label">
                                                         <input type="checkbox" class="form-check-input" aria-checked="false"><i class="input-helper"></i></label>
                                                      </div>
                                                    </td>-->
                                                    <tr>
                                                    <td>
                                                    <div class="d-flex">
                                                      <img src="./assets/images/faces/'. $row['Profile_Pic'] .'" alt="">
                                                      <div>
                                                        <h6>'. $row['F_Name'] . ' ' .$row['L_Name'] . '</h6>
                                                        <p>'. $row['Dept'] .'</p>
                                                      </div>
                                                    </div>
                                                    </td>
                                                    <td><div class="badge badge-opacity-'. $temp .'">'. $fin .'</div></td>
                                                  </tr>';
                                          }
                                          echo '</tbody>
                                            </table>'; 
                                          } else {
                                              echo '0 Result';
                                            }
                                          $conn-> close();
                                        ?>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                       <!--  <div class="row flex-grow">
                          <div class="col-12 grid-margin stretch-card">
                            <div class="card card-rounded">
                              <div class="card-body">
                                <div class="row">
                                  <div class="col-lg-12">
                                    <div class="d-flex justify-content-between align-items-center mb-3">
                                      <h4 class="card-title card-title-dash">Type By Amount</h4>
                                    </div>
                                    <canvas class="my-auto" id="doughnutChart" height="200"></canvas>
                                    <div id="doughnut-chart-legend" class="mt-5 text-center"></div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div> -->
                       <!--  <div class="row flex-grow">
                          <div class="col-12 grid-margin stretch-card">
                            <div class="card card-rounded">
                              <div class="card-body">
                                <div class="row">
                                  <div class="col-lg-12">
                                    <div class="d-flex justify-content-between align-items-center mb-3">
                                      <div>
                                        <h4 class="card-title card-title-dash">Leave Report</h4>
                                      </div>
                                      <div>
                                        <div class="dropdown">
                                          <button class="btn btn-secondary dropdown-toggle toggle-dark btn-lg mb-0 me-0" type="button" id="dropdownMenuButton3" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Month Wise </button>
                                          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton3">
                                            <h6 class="dropdown-header">week Wise</h6>
                                            <a class="dropdown-item" href="#">Year Wise</a>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="mt-3">
                                      <canvas id="leaveReport"></canvas>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div> -->
                        <div class="row flex-grow">
                          <div class="col-12 grid-margin stretch-card">
                            <div class="card card-rounded">
                              <div class="card-body">
                                <div class="row">
                                  <div class="col-lg-12">
                                    <div class="d-flex justify-content-between align-items-center mb-3">
                                      <div>
                                        <h4 class="card-title card-title-dash">Top Zones</h4>
                                      </div>
                                    </div>
                                    <div class="mt-3">
                                      <div class="wrapper d-flex align-items-center justify-content-between py-2 border-bottom">
                                        <div class="d-flex">
                                          <img class="img-sm rounded-10" src="./assets/images/loc/loc3.jpg" alt="profile">
                                          <div class="wrapper ms-3">
                                            <p class="ms-1 mb-1 fw-bold">Maninagar</p>
                                            <small class="text-muted mb-0"><font color="green">162543</font></small>
                                          </div>
                                        </div>
                                        <div class="text-muted text-small">
                                          <font color="red">250</font>
                                        </div>
                                      </div>
                                      <div class="wrapper d-flex align-items-center justify-content-between py-2 border-bottom">
                                        <div class="d-flex">
                                          <img class="img-sm rounded-10" src="./assets/images/loc/loc4.jpg" alt="profile">
                                          <div class="wrapper ms-3">
                                            <p class="ms-1 mb-1 fw-bold">Behrampura</p>
                                            <small class="text-muted mb-0"><font color="green">152543</font></small>
                                          </div>
                                        </div>
                                        <div class="text-muted text-small">
                                          <font color="red">200</font>
                                        </div>
                                      </div>
                                      <div class="wrapper d-flex align-items-center justify-content-between py-2 border-bottom">
                                        <div class="d-flex">
                                          <img class="img-sm rounded-10" src="./assets/images/loc/loc5.jpg" alt="profile">
                                          <div class="wrapper ms-3">
                                            <p class="ms-1 mb-1 fw-bold">Naroda</p>
                                            <small class="text-muted mb-0"><font color="green">142543</font></small>
                                          </div>
                                        </div>
                                        <div class="text-muted text-small">
                                          <font color="red">150</font>
                                        </div>
                                      </div>
                                      <div class="wrapper d-flex align-items-center justify-content-between py-2 border-bottom">
                                        <div class="d-flex">
                                          <img class="img-sm rounded-10" src="./assets/images/loc/loc2.jpg" alt="profile">
                                          <div class="wrapper ms-3">
                                            <p class="ms-1 mb-1 fw-bold">Chandranagar</p>
                                            <small class="text-muted mb-0"><font color="green">132543</font></small>
                                          </div>
                                        </div>
                                        <div class="text-muted text-small">
                                          <font color="red">100</font>
                                        </div>
                                      </div>
                                      <div class="wrapper d-flex align-items-center justify-content-between pt-2">
                                        <div class="d-flex">
                                          <img class="img-sm rounded-10" src="./assets/images/loc/loc1.jpg" alt="profile">
                                          <div class="wrapper ms-3">
                                            <p class="ms-1 mb-1 fw-bold">Sharkhej</p>
                                            <small class="text-muted mb-0"><font color="green">122543</font></small>
                                          </div>
                                        </div>
                                        <div class="text-muted text-small">
                                          <font color="red">50</font>
                                        </div>
                                      </div>
                                      <?php
                                        $conn = mysqli_connect("localhost", "root", "", "test");
                        
                                        // Check connection
                                        if($conn === false){
                                            die("ERROR: Could not connect. " 
                                                . mysqli_connect_error());
                                        }

                                        $sql = "SELECT Z_Name, T_Comp, R_Comp, Profile_Pic from zones";
                                        $result = $conn-> query($sql);

                                        if ($result-> num_rows > 0){
                                          while ($row = $result-> fetch_assoc()){
                                            echo '<div class="wrapper d-flex align-items-center justify-content-between pt-2">
                                                  <div class="d-flex">
                                                    <img class="img-sm rounded-10" src="./assets/images/loc/'. $row['Profile_Pic'] .'" alt="profile">
                                                    <div class="wrapper ms-3">
                                                      <p class="ms-1 mb-1 fw-bold">'. $row['Z_Name'] .'</p>
                                                      <small class="text-muted mb-0"><font color="green">'. $row['T_Comp'] .'</font></small>
                                                    </div>
                                                  </div>
                                                  <div class="text-muted text-small">
                                                    <font color="red">'. $row['R_Comp'] .'</font>
                                                  </div>
                                                </div>';
                                                } 
                                        } else {
                                            echo '0 Result';
                                          }
                                        $conn-> close();
                                      ?>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        
<?php
include'./inc/footer.php';
?>