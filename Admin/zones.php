<?php
include './inc/header.php';
include './inc/sidebar.php';
?>
<!DOCTYPE html>
<html>
<head>
	<title>Zone Management</title>
</head>
<body>
<div class="row flex-grow">
                          <div class="col-12 grid-margin stretch-card">
                            <div class="card card-rounded">
                              <div class="card-body">
                                <div class="row">
                                  <div class="col-lg-12">
                                    <div class="d-flex justify-content-between align-items-center mb-3">
                                      <div>
                                        <h4 class="card-title card-title-dash">Top Zones</h4>
                                      </div>
                                      <div>
                                    <a href="./Create_Zone.html"><button  class="btn btn-primary btn-lg text-white mb-0 me-0" type="button"><i class="mdi mdi-account-plus"></i> Add Zones </button> </a>
                                  </div>
                                    </div>
                                    <div class="mt-3">
                                      <div class="wrapper d-flex align-items-center justify-content-between py-2 border-bottom">
                                        <div class="d-flex">
                                          <img class="img-sm rounded-10" src="./assets/images/loc/loc3.jpg" alt="profile">
                                          <div class="wrapper ms-3">
                                            <p class="ms-1 mb-1 fw-bold">Maninagar</p>
                                            <small class="text-muted mb-0"><font color="green">Total Complaints: 162543</font></small>
                                          </div>
                                        </div>
                                        <div class="text-muted text-small">
                                          <font color="red">Remaining Complaints: 250</font>
                                        </div>
                                      </div>
                                      <div class="wrapper d-flex align-items-center justify-content-between py-2 border-bottom">
                                        <div class="d-flex">
                                          <img class="img-sm rounded-10" src="./assets/images/loc/loc4.jpg" alt="profile">
                                          <div class="wrapper ms-3">
                                            <p class="ms-1 mb-1 fw-bold">Behrampura</p>
                                            <small class="text-muted mb-0"><font color="green">Total Complaints: 152543</font></small>
                                          </div>
                                        </div>
                                        <div class="text-muted text-small">
                                          <font color="red">Remaining Complaints: 200</font>
                                        </div>
                                      </div>
                                      <div class="wrapper d-flex align-items-center justify-content-between py-2 border-bottom">
                                        <div class="d-flex">
                                          <img class="img-sm rounded-10" src="./assets/images/loc/loc5.jpg" alt="profile">
                                          <div class="wrapper ms-3">
                                            <p class="ms-1 mb-1 fw-bold">Naroda</p>
                                            <small class="text-muted mb-0"><font color="green">Total Complaints: 142543</font></small>
                                          </div>
                                        </div>
                                        <div class="text-muted text-small">
                                          <font color="red">Remaining Complaints: 150</font>
                                        </div>
                                      </div>
                                      <div class="wrapper d-flex align-items-center justify-content-between py-2 border-bottom">
                                        <div class="d-flex">
                                          <img class="img-sm rounded-10" src="./assets/images/loc/loc2.jpg" alt="profile">
                                          <div class="wrapper ms-3">
                                            <p class="ms-1 mb-1 fw-bold">Chandranagar</p>
                                            <small class="text-muted mb-0"><font color="green">Total Complaints: 132543</font></small>
                                          </div>
                                        </div>
                                        <div class="text-muted text-small">
                                          <font color="red">Remaining Complaints: 100</font>
                                        </div>
                                      </div>
                                      <div class="wrapper d-flex align-items-center justify-content-between py-2 border-bottom">
                                        <div class="d-flex">
                                          <img class="img-sm rounded-10" src="./assets/images/loc/loc1.jpg" alt="profile">
                                          <div class="wrapper ms-3">
                                            <p class="ms-1 mb-1 fw-bold">Sharkhej</p>
                                            <small class="text-muted mb-0"><font color="green">Total Complaints: 122543</font></small>
                                          </div>
                                        </div>
                                        <div class="text-muted text-small">
                                          <font color="red">Remaining Complaints: 50</font>
                                        </div>
                                      </div>
                                      <?php
                                        $conn = mysqli_connect("localhost", "root", "", "test");
                        
                                        // Check connection
                                        if($conn === false){
                                            die("ERROR: Could not connect. " 
                                                . mysqli_connect_error());
                                        }

                                        $sql = "SELECT Z_Name, T_Comp, R_Comp, Profile_Pic from zones";
                                        $result = $conn-> query($sql);

                                        if ($result-> num_rows > 0){
                                          while ($row = $result-> fetch_assoc()){
                                            echo '<div class="wrapper d-flex align-items-center justify-content-between py-2 border-bottom">
                                                    <div class="d-flex">
                                                      <img class="img-sm rounded-10" src="./assets/images/loc/'. $row['Profile_Pic'] .'" alt="profile">
                                                      <div class="wrapper ms-3">
                                                        <p class="ms-1 mb-1 fw-bold">'. $row['Z_Name'] .'</p>
                                                        <small class="text-muted mb-0"><font color="green">Total Complaints: '. $row['T_Comp'] .'</font></small>
                                                      </div>
                                                    </div>
                                                    <div class="text-muted text-small">
                                                      <font color="red">Remaining Complaints:'. $row['R_Comp'] .'</font>
                                                    </div>
                                                  </div>';
                                                } 
                                        } else {
                                            echo '0 Result';
                                          }
                                        $conn-> close();
                                      ?>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
</body>
</html>