<?php
include './inc/header.php';
include './inc/sidebar.php';
?>

<!DOCTYPE html>
<html>
<head>
	<title>User Management</title>
</head>
<body>
<div class="row flex-grow">
                          <div class="col-12 grid-margin stretch-card">
                            <div class="card card-rounded">
                              <div class="card-body">
                                <div class="d-sm-flex justify-content-between align-items-start">
                                  <div>
                                    <h4 class="card-title card-title-dash">Sub Admins</h4>
                                   <p class="card-subtitle card-subtitle-dash">
                                     <?php
                                     $conn = mysqli_connect("localhost", "root", "", "test");
                        
                                     // Check connection
                                     if($conn === false){
                                         die("ERROR: Could not connect. " 
                                             . mysqli_connect_error());
                                     }

                                     $sql = "SELECT * from sub_admins";
                                     $result = $conn-> query($sql); 
                                     
                                     $rowcount = mysqli_num_rows($result);
                                     echo (5+ ($rowcount)) .' Sub Admins</p>';
                                     $conn-> close();

                                     ?>
                                  </div>
                                  <div>
                                    <a href="./Create_Sub.html"><button  class="btn btn-primary btn-lg text-white mb-0 me-0" type="button"><i class="mdi mdi-account-plus"></i> Add Sub-Admins</button></a> 
                                  </div>
                                </div>
                                <div class="table-responsive  mt-1">
                                  <table class="table select-table">
                                    <thead>
                                      <tr>
                                        <th>
                                          <div class="form-check form-check-flat mt-0">
                                            <label class="form-check-label">
                                              <!-- <input type="checkbox" class="form-check-input" aria-checked="false"><i class="input-helper"></i></label> -->
                                          </div>
                                        </th>
                                        <th>Sub Admins</th>
                                        <th>Departments</th>
                                        <th>Progress</th>
                                        <th>Status</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td>
                                          <div class="form-check form-check-flat mt-0">
                                            <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" aria-checked="false"><i class="input-helper"></i></label>
                                          </div>
                                        </td>
                                        <td>
                                          <div class="d-flex ">
                                            <!-- <img src="./assets/images/faces/face1.jpg" alt=""> -->
                                            <div>
                                              <h6>Brandon Washington</h6>
                                              <p>Head Admin</p>
                                            </div>
                                          </div>
                                        </td>
                                        <td>
                                          <h6>AMC</h6>
                                          <p>Government Co-operation</p>
                                        </td>
                                        <td>
                                          <div>
                                            <div class="d-flex justify-content-between align-items-center mb-1 max-width-progress-wrap">
                                              <p class="text-success">79%</p>
                                              <p>85/162</p>
                                            </div>
                                            <div class="progress progress-md">
                                              <div class="progress-bar bg-success" role="progressbar" style="width: 85%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                          </div>
                                        </td>
                                        <td><div class="badge badge-opacity-warning">In progress</div></td>
                                      </tr>
                                      <tr>
                                        <td>
                                          <div class="form-check form-check-flat mt-0">
                                            <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" aria-checked="false"><i class="input-helper"></i></label>
                                          </div>
                                        </td>
                                        <td>
                                          <div class="d-flex">
                                           <!--  <img src="./assets/images/faces/face2.jpg" alt=""> -->
                                            <div>
                                              <h6>Laura Brooks</h6>
                                              <p>Sub Admin</p>
                                            </div>
                                          </div>
                                        </td>
                                        <td>
                                          <h6>AMC Water Department</h6>
                                          <p>Government Department</p>
                                        </td>
                                        <td>
                                          <div>
                                            <div class="d-flex justify-content-between align-items-center mb-1 max-width-progress-wrap">
                                              <p class="text-success">65%</p>
                                              <p>85/162</p>
                                            </div>
                                            <div class="progress progress-md">
                                              <div class="progress-bar bg-success" role="progressbar" style="width: 65%" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                          </div>
                                        </td>
                                        <td><div class="badge badge-opacity-warning">In progress</div></td>
                                      </tr>
                                      <tr>
                                        <td>
                                          <div class="form-check form-check-flat mt-0">
                                            <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" aria-checked="false"><i class="input-helper"></i></label>
                                          </div>
                                        </td>
                                        <td>
                                          <div class="d-flex">
                                           <!--  <img src="./assets/images/faces/face3.jpg" alt=""> -->
                                            <div>
                                              <h6>Wayne Murphy</h6>
                                              <p>Sub Admin</p>
                                            </div>
                                          </div>
                                        </td>
                                        <td>
                                          <h6>AMC Electricity Department</h6>
                                          <p>Private Institution : Torrent</p>
                                        </td>
                                        <td>
                                          <div>
                                            <div class="d-flex justify-content-between align-items-center mb-1 max-width-progress-wrap">
                                              <p class="text-success">65%</p>
                                              <p>85/162</p>
                                            </div>
                                            <div class="progress progress-md">
                                              <div class="progress-bar bg-warning" role="progressbar" style="width: 38%" aria-valuenow="38" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                          </div>
                                        </td>
                                        <td><div class="badge badge-opacity-warning">In progress</div></td>
                                      </tr>
                                      <tr>
                                        <td>
                                          <div class="form-check form-check-flat mt-0">
                                            <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" aria-checked="false"><i class="input-helper"></i></label>
                                          </div>
                                        </td>
                                        <td>
                                          <div class="d-flex">
                                            <!-- <img src="./assets/images/faces/face4.jpg" alt=""> -->
                                            <div>
                                              <h6>Matthew Bailey</h6>
                                              <p>Sub Admin</p>
                                            </div>
                                          </div>
                                        </td>
                                        <td>
                                          <h6>AMC Transportation Department</h6>
                                          <p>Government Department</p>
                                        </td>
                                        <td>
                                          <div>
                                            <div class="d-flex justify-content-between align-items-center mb-1 max-width-progress-wrap">
                                              <p class="text-success">65%</p>
                                              <p>85/162</p>
                                            </div>
                                            <div class="progress progress-md">
                                              <div class="progress-bar bg-danger" role="progressbar" style="width: 15%" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                          </div>
                                        </td>
                                        <td><div class="badge badge-opacity-danger">Pending</div></td>
                                      </tr>
                                      <tr>
                                        <td>
                                          <div class="form-check form-check-flat mt-0">
                                            <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" aria-checked="false"><i class="input-helper"></i></label>
                                          </div>
                                        </td>
                                        <td>
                                          <div class="d-flex">
                                            <!-- <img src="./assets/images/faces/face5.jpg" alt=""> -->
                                            <div>
                                              <h6>Katherine Butler</h6>
                                              <p>Sub Admin</p>
                                            </div>
                                          </div>
                                        </td>
                                        <td>
                                          <h6>AMC Cleaning and Sanitization Department</h6>
                                          <p>Government Department</p>
                                        </td>
                                        <td>
                                          <div>
                                            <div class="d-flex justify-content-between align-items-center mb-1 max-width-progress-wrap">
                                              <p class="text-success">65%</p>
                                              <p>85/162</p>
                                            </div>
                                            <div class="progress progress-md">
                                              <div class="progress-bar bg-success" role="progressbar" style="width: 65%" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                          </div>
                                        </td>
                                        <td><div class="badge badge-opacity-success">Completed</div></td>
                                      </tr>
                                      <?php
                                        $conn = mysqli_connect("localhost", "root", "", "test");
                        
                                        // Check connection
                                        if($conn === false){
                                            die("ERROR: Could not connect. " 
                                                . mysqli_connect_error());
                                        }

                                        $sql = "SELECT F_Name, L_Name, Dept, Sector, Position from sub_admins";
                                        $result = $conn-> query($sql);

                                        if ($result-> num_rows > 0){
                                          while ($row = $result-> fetch_assoc()){
                                            $n=rand( 1 , 100);
                                            $n2=rand( 100 , 200);
                                            $a=array("warning"=>"in progress","danger"=>"Pending","success" => "Completed");
                                            
                                            $temp=array_rand($a,1);
                                            $fin = (strcmp($temp,"suceess") && strcmp($temp,"danger") ) ? (strcmp($temp,"warning") ? "Completed" : "in Progress") :"Pending";
                                            
                                            echo '<tr>
                                                    <td>
                                                      <div class="form-check form-check-flat mt-0">
                                                        <label class="form-check-label">
                                                        <input type="checkbox" class="form-check-input" aria-checked="false"><i class="input-helper"></i></label>
                                                      </div>
                                                    </td>
                                                    <td>
                                                      <div class="d-flex">
                                                        <!-- <img src="./assets/images/faces/face5.jpg" alt=""> -->
                                                        <div>
                                                          <h6>'. $row['F_Name'] . ' ' . $row['L_Name'] . '</h6>
                                                          <p>'. $row['Position'] . '</p>
                                                        </div>
                                                      </div>
                                                    </td> 
                                                    <td>
                                                      <h6>'. $row['Dept'] . '</h6>
                                                      <p>'. $row['Sector'] . '</p>
                                                    </td>
                                                    <td>
                                                      <div>
                                                        <div class="d-flex justify-content-between align-items-center mb-1 max-width-progress-wrap">
                                                          <p class="text-'. $temp .'">'. $n .' %</p>
                                                          <p>'. $n .'/'. $n2 .'</p>
                                                        </div>
                                                        <div class="progress progress-md">
                                                          <div class="progress-bar bg-'. $temp .'" role="progressbar" style="width: '. $n .'%" aria-valuenow="'. $n .'" aria-valuemin="0" aria-valuemax="100"></div>
                                                        </div>
                                                      </div>
                                                    </td>
                                                    <td><div class="badge badge-opacity-'. $temp .'">'. $fin .' </div></td>
                                                  </tr>';
                                                }
                                                echo '</tbody>
                                                  </table>'; 
                                        } else {
                                            echo '0 Result';
                                          }
                                        $conn-> close();
                                      ?>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
</body>
</html>

<!-- <?php
// include './inc/footer.php';
?> -->